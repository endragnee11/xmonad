#! /usr/bin/env bash

chmod +x ./installers/*

echo "Choose your distro 1) Arch 2) Debian 3) Ubuntu 4) Fedora"

read -r -p "Choose your distro(default 1)(will not re-install): " distro

case $distro in 
[1])
	DRI='./installers/install-on-arch.sh'
	;;

[2])
	DRI='./installers/install-on-debian.sh'
	;;

[3])
    DRI='./installers/install-on-ubuntu.sh'
    ;;

[4])
	DRI="./installers/install-on-fedora.sh"
	;;
esac

#Installing Configs
echo "Installing the Configs"
./installers/config.sh



