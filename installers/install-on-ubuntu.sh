#! /usr/bin/env bash
set -e

echo "---------------------------------------------------------------------------------------------"
echo "Installing neccesay tools with apt"
echo "---------------------------------------------------------------------------------------------"
echo "------------------Adding neccesay repos------------------------------------------------------"

sudo add-apt-repository ppa:mmstick76/alacritty
sudo dpkg --add-architecture i386 && sudo apt update && sudo apt upgrade

sleep 2
#----------------------apt commands-----------------------#
#sudo apt install neovim curl git wget feh rofi xorg xmonad acpi wmctrl brightnessctl playerctl dunst jq xclip conky maim xautolock nautilus apt-transport-https libxcb-util-dev libxext-dev libxcb2-dev libxcb-damage0-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-render-util0-dev libxcb-render0-dev libxcb-randr0-dev libxcb-composite0-dev libxcb-image0-dev libxcb-present-dev libxcb-xinerama0-dev libxcb-glx0-dev libpixman-1-dev libdbus-1-dev libconfig-dev libgl1-mesa-dev libpcre2-dev libevdev-dev uthash-dev libev-dev libx11-xcb-dev meson libsystemd-dev pkg-config ninja-build libdbus-1-dev libinih-dev dbus-user-session asciidoc cmake libpcre++-dev lxappearance libltdl-dev libxcb-xkb-dev libxcb-xtest0-dev zsh alacritty imagemagick apt-transport-https celluloid ranger nodejs
#new deps
sudo apt install neovim curl git wget feh rofi xmonad acpi wmctrl brightnessctl playerctl dunst jq xclip conky maim xautolock nautilus lxappearance zsh alacritty imagemagick celluloid ranger nodejs meson autoconf ninja gcc python make pkg-config libpam0g-dev libcairo2-dev libfontconfig1-dev libxcb-composite0-dev libev-dev libx11-xcb-dev libxcb-xkb-dev libxcb-xinerama0-dev libxcb-randr0-dev libxcb-image0-dev libxcb-util-dev libxcb-xrm-dev libxkbcommon-dev libxkbcommon-x11-dev libjpeg-dev libxext-dev libxcb1-dev libxcb-damage0-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-render-util0-dev libxcb-render0-dev libxcb-randr0-dev libxcb-composite0-dev libxcb-image0-dev libxcb-present-dev libxcb-xinerama0-dev libxcb-glx0-dev libpixman-1-dev libdbus-1-dev libconfig-dev libgl1-mesa-dev  libpcre2-dev  libevdev-dev uthash-dev libev-dev libx11-xcb-dev

#choose video drivers
#echo "1) xf86-video-intel   2) xf86-video-amdgpu 3) nvidia 4) Skip"
#read -r -p "Choose you video card driver(default 1)(will not re-install): " vid


echo "--------------------------Git cloning neccesary tools"

mkdir ~/opt && mkdir ~/opt/git
cd ~/opt/git

echo "Git cloning picom"

if [ -d ~/opt/git/picom ]; then
    echo "Picom is detected installing"
    cd picom;
    meson --buildtype=release . build;
    ninja -C build;
# To install the binaries in /usr/local/bin (optional)
    sudo ninja -C build install;
else
    echo "Installing picom"
    git clone https://github.com/jonaburg/picom;
    cd picom;
    meson --buildtype=release . build;
    ninja -C build;
# To install the binaries in /usr/local/bin (optional)
    sudo ninja -C build install;
fi

cd ~/opt/git

if [ -d /usr/share/icons/candy-icons ]; then
    echo "Candy icons are found ..."
else
    echo "Git cloning candy icons"
    git clone https://github.com/EliverLara/candy-icons.git
    sudo cp -r candy-icons/ /usr/share/icons/
fi

sleep 3

cd ~/opt/git

if [ -d ~/opt/git/betterlockscreen-main ]; then
    echo "Betterlockscreen is detected ..."
    cd betterlockscreen-main/
    sudo rm /usr/bin/betterlockscreen
    sudo cp betterlockscreen /usr/bin
else
    echo "Installing betterlockscreen"
#Installing betterlockscreen
    wget https://github.com/pavanjadhaw/betterlockscreen/archive/refs/heads/main.zip
unzip main.zip
    cd betterlockscreen-main/
    sudo cp betterlockscreen /usr/bin
fi

cd ~/opt/git

if [ -d ~/opt/git/i3lock-color ]; then
    echo "i3lock-color is detected ..."
    cd i3lock-color
    ./install-i3lock-color.sh
else
    echo "Installing i3lock-color"
#i3lock-color
    git clone https://github.com/Raymo111/i3lock-color.git
    cd i3lock-color
    ./install-i3lock-color.sh
fi

cd ~/opt/git

#color shell script
if [ -d ~/opt/git/shell-color-scripts ]; then
    echo "shell-color-scripts is detected ..."
    cd shell-color-scripts
    rm -rf /opt/shell-color-scripts || return 1
    sudo mkdir -p /opt/shell-color-scripts/colorscripts || return 1
    sudo cp -rf colorscripts/* /opt/shell-color-scripts/colorscripts
    sudo cp colorscript.sh /usr/bin/colorscript
else
    echo "Installing shell-color-scripts"
    git clone https://gitlab.com/dwt1/shell-color-scripts.git
    cd shell-color-scripts
    rm -rf /opt/shell-color-scripts || return 1
    sudo mkdir -p /opt/shell-color-scripts/colorscripts || return 1
    sudo cp -rf colorscripts/* /opt/shell-color-scripts/colorscripts
    sudo cp colorscript.sh /usr/bin/colorscript
fi

# optional for zsh completion
sudo cp zsh_completion/_colorscript /usr/share/zsh/site-functions


cd ~/opt/git

# echo "Installing zsh"
echo "Installing nvim config"
#nvim
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
mkdir -p ~/.cache/nvim

echo "Installing brave"
#Installing brave
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update
sudo apt install brave-browser
#Installing libxcb-shape0-dev
echo "Installing lsd"
wget "https://github.com/Peltoche/lsd/releases/download/0.20.1/lsd-musl_0.20.1_amd64.deb"
sudo dpkg -i lsd-musl_0.20.1_amd64.deb

#Installing gamming tools
~/opt/xmonad/installers/gaming-on-ubuntu.sh
