#! /usr/bin/env bash
#Only for mainline kernel

sudo pacman -S --needed nvidia-dkms nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader xf86-video-nouveau
sudo pacman -S optimus-manager optimus-manager-qt bbswitch-dkms
#For non-mainline kernel
#sudo pacman -S --needed lib32-mesa vulkan-intel lib32-vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader